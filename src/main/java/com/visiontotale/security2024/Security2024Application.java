package com.visiontotale.security2024;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Security2024Application {

	public static void main(String[] args) {
		SpringApplication.run(Security2024Application.class, args);
	}

}
